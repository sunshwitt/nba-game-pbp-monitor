import time
from datetime import datetime, timedelta

from sql_helper.sql_command_executer import SQLCommandExecuter
from xml_helper.xml_parser import XMLParser
from external_helper.external_command_executer import ExternalCommandExecuter
from multiprocessing.pool import ThreadPool
from slack_notification_helper.send_process_notification import print_and_send_slack_notification
from r_script_locations import r_script_halftime_sheet_path, r_script_end_of_game_sheet_path, r_script_postgame_stats_path, r_script_next_pregame_path


class GameMonitor:

    def __init__(self):
        self.sql_command_executer = SQLCommandExecuter('NBA')
        self.sql_command_executer.initialize_session()
        self.game_id, self.game_date, self.away_team, self.home_team, self.date_today = self.sql_command_executer.get_game_info_for_next_suns_game()
        self.external_executer = ExternalCommandExecuter()
        self.pbp_load_statement = self.print_and_return_load_statement_string()
        self.last_quarter_of_game = 'Q4'

    def print_and_return_load_statement_string(self):
        pbp_load_statement = '//phxsports.local/Shares/BBallOpsData/shared-neo-data-pipeline/runners/nba_runners/' \
                             'nba_game_specific_file_runner.py --gameid=' + self.game_id
        print('Updated pbp load statement to: {}'.format(pbp_load_statement))
        return pbp_load_statement

    # Function to start multithreaded process for checking for end of second and fourth quarter files simultaneously
    #   will run two calls to "start_individual_monitor_process" in parallel
    def start_threaded_monitor_process(self):
        pool = ThreadPool()
        pool.map(self.start_individual_monitor_process, ['Q2', 'Q4'])

    def start_individual_monitor_process(self, quarter_to_monitor_end_of):
        file_name = "//alderaan/NBA_Stats/" + self.game_id + "_pbp_" + quarter_to_monitor_end_of + ".xml"
        xml_parser = XMLParser(file_name)
        run_game_load_done = False
        ss_is_loaded = False
        while 1 == 1:
            if not run_game_load_done:
                print('{}: Checking for new files for game_id: {} on {}: {} at {} ({})\n'
                      .format(time.ctime(), self.game_id, self.game_date, self.away_team, self.home_team, quarter_to_monitor_end_of))
            elif not ss_is_loaded and quarter_to_monitor_end_of != 'Q2':
                print('{}: Checking for SS data for game_id: {}'.format(time.ctime(), self.game_id))
            else:
                print('Game loaded for tonight ({}); waiting until end of day to find new game id\n'.format(self.game_id))
            is_end_of_period, is_tie_game = xml_parser.check_xml_file_for_end_of_period_and_tie_game()
            if is_end_of_period and not run_game_load_done:
                print_and_send_slack_notification('LETS BAKE SOME REPORTS BOIII')
                print_and_send_slack_notification('Files found! Running load of PBP data from the data pipeline {}...\n'.format(
                    quarter_to_monitor_end_of))
                self.external_executer.run_external_command(self.pbp_load_statement, True)
                print_and_send_slack_notification('Done with PBP load to Snowflake; executing R script {}...\n'.format(quarter_to_monitor_end_of))
                self.external_executer.run_external_command(r_script_halftime_sheet_path, False) \
                    if quarter_to_monitor_end_of == 'Q2' \
                    else self.external_executer.run_external_command(r_script_end_of_game_sheet_path, False)
                site_location = 'https://suns-apps.shinyapps.io/halftime_sheet/' if quarter_to_monitor_end_of == 'Q2' \
                    else 'https://suns-apps.shinyapps.io/eog_sheet/'
                print_and_send_slack_notification('Process done! Report available at: ' + site_location)
                run_game_load_done = True
                if quarter_to_monitor_end_of == self.last_quarter_of_game:
                    if is_tie_game:
                        self.last_quarter_of_game = 'Q' + str((int(self.last_quarter_of_game[1]) + 1))
                        file_name = "//alderaan/NBA_Stats/" + self.game_id + "_pbp_" + self.last_quarter_of_game + ".xml"
                        xml_parser.set_file_name(file_name)
                        run_game_load_done = False
                        quarter_to_monitor_end_of = self.last_quarter_of_game
                    else:
                        print_and_send_slack_notification('Running postgame stats')
                        self.external_executer.run_external_command(r_script_postgame_stats_path, False)
                        print_and_send_slack_notification('Done with postgame stats')
            elif is_end_of_period and not ss_is_loaded and quarter_to_monitor_end_of != 'Q2':
                ss_is_loaded = self.sql_command_executer.poll_database_for_ss_data(self.game_id)
                if ss_is_loaded:
                    print('Second Spectrum loaded - running postgame stats again')
                    self.external_executer.run_external_command(r_script_postgame_stats_path, False)
                    print_and_send_slack_notification('Done with postgame stats v2')
                    self.external_executer.run_external_command(r_script_next_pregame_path, False)
                    print_and_send_slack_notification('Done with pregame report run')
                time.sleep(50)
            if run_game_load_done and ss_is_loaded and datetime.strptime(self.date_today, '%Y-%m-%d') + timedelta(days=1, hours=4) < datetime.now():
                if int(self.last_quarter_of_game[1]) > 4:
                    self.last_quarter_of_game = 'Q4'
                    quarter_to_monitor_end_of = 'Q4'
                run_game_load_done = False
                self.sql_command_executer.initialize_session()
                self.game_id, self.game_date, self.away_team, self.home_team, self.date_today = \
                    self.sql_command_executer.get_game_info_for_next_suns_game()
                self.pbp_load_statement = self.print_and_return_load_statement_string()
                file_name = "//alderaan/NBA_Stats/" + self.game_id + "_pbp_" + quarter_to_monitor_end_of + ".xml"
                xml_parser.set_file_name(file_name)
            time.sleep(10)
