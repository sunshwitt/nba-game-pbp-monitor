import subprocess
import shlex
import sys


class ExternalCommandExecuter:
    def __init__(self):
        pass

    def run_external_command(self, command, is_python):
        if is_python:
            steps_to_execute = ['cmd.exe', '/c', sys.executable]
        else:
            steps_to_execute = ['cmd.exe', '/c']
        steps_to_execute.extend(shlex.split(command))
        pid = subprocess.Popen(steps_to_execute, shell=True, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        out, err = pid.communicate()
        print(str(out, "utf-8") + ' ' + str(err, 'utf-8'))
        return pid
