# NBA Game PBP Monitor #

last updated 2/24/2020

### Purpose ###

This project is run continuously on one of our virtual machines in 
order to keep track of when games hit halftime / postgame, with the 
end goal being automating those processes from the data side.

### How do I get set up? ###

The project should be running at all times on one of our virtual machines;
locally, you should be able to open the project in Pycharm and create a 
configuration to run the "game_monitor_runner.py" script and run it that way.

A Note: in the "game_monitor" script the "r_script_statement" variable may 
need to be tweaked based on your installation location for R; for example,
for some reason on my personal machine it installed it under documents instead 
of Program Files.

### Technologies Involved ###

* XML ElementTree for XML parsing
* Multiprocessing threadpools for running 2nd / 4th quarter monitors in
parallel
* SQLAlchemy for SQL command execution
* Subprocess calling to execute external Python command