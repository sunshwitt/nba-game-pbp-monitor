import pandas as pd
import sqlalchemy
import sqlalchemy.orm
import datetime
from snowflake.sqlalchemy import URL
from snowflake_credentials import snowflake_usr, snowflake_pwd


class SQLCommandExecuter:
    def __init__(self, schema):
        self.snowflake_usr = snowflake_usr
        self.snowflake_pwd = snowflake_pwd
        self.snowflake_act = 'suns'
        self.database = 'SUNS_PROD'
        self.schema = schema
        self.warehouse = 'DEMO_WH'
        self.data_dir_stage = '//phxsports.local/Shares/BBallOpsData/Data/SnowflakeStage/'
        self.snowflake_engine = None

    def initialize_session(self):
        self.snowflake_engine = sqlalchemy.create_engine(URL(
            account=self.snowflake_act,
            user=self.snowflake_usr,
            password=self.snowflake_pwd,
            database=self.database,
            schema=self.schema,
            warehouse=self.warehouse,
            role='ACCOUNTADMIN'
        ))
        use_warehouse_command = 'USE WAREHOUSE {};'.format(self.warehouse)
        use_db_command = 'USE DATABASE {};'.format(self.database)
        use_schema_command = 'USE SCHEMA {};'.format(self.schema)
        with self.snowflake_engine.connect() as connection:
            connection.execute(use_warehouse_command)
            connection.execute(use_db_command)
            connection.execute(use_schema_command)

    def get_game_info_for_next_suns_game(self):
        date_today = datetime.datetime.today().strftime('%Y-%m-%d')
        game_info_sql_statement = """
        select top 1 game_id, game_date, away_team_city, home_team_city
             from nba.nba_schedule_game_info
            where (away_team_city = 'Phoenix' or home_team_city = 'Phoenix')
            and game_date >= '{}'
            and league_id = '00'
            order by game_date asc """.format(date_today)
        with self.snowflake_engine.connect() as connection:
            result = connection.execute(game_info_sql_statement)
            game_id, game_date, away_team, home_team = result.first()
        return game_id, game_date, away_team, home_team, date_today

    def poll_database_for_ss_data(self, nba_game_id):
        poll_sql_stmt = """
                        select top 1 *
                        from second_spectrum.ss_chance_players c, second_spectrum.ss_meta_games g
                        where c.ss_game_id = g.ss_game_id
                        and g.nba_game_id = '{}'
                        """.format(nba_game_id)
        sql_results_df = pd.read_sql(sqlalchemy.sql.text(poll_sql_stmt), self.snowflake_engine)
        return not sql_results_df.empty
