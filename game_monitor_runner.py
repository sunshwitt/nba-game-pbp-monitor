from game_monitor import GameMonitor


def main():
    halftime_and_end_of_game_monitor = GameMonitor()
    halftime_and_end_of_game_monitor.start_threaded_monitor_process()


if __name__ == '__main__':
    main()
