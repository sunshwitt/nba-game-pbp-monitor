import xml.etree.ElementTree as et
import os.path


class XMLParser:
    def __init__(self, file_name):
        self.file_name = file_name

    def check_xml_file_for_end_of_period_and_tie_game(self):
        found_end_of_period = False
        score_is_tied = False
        if os.path.exists(self.file_name):
            with open(self.file_name) as fp:
                try:
                    elem = et.parse(fp).getroot()[0][0]
                    for node in elem.findall('Event_pbp'):
                        clock = node.attrib['Game_clock']
                        descr = node.attrib['Description']
                        if (clock == "00:00" or clock == "00:00.0")\
                                and descr == 'End Period':
                            found_end_of_period = True
                            away_score = int(node.attrib['Visitor_score'])
                            home_score = int(node.attrib['Home_score'])
                            if home_score == away_score:
                                score_is_tied = True
                except Exception as e:
                    print("idk something stupid happened when parsing XML. Error data...\n")
                    print(e+"\n Resuming searching for file updates...")
                finally:
                    return found_end_of_period, score_is_tied
        return found_end_of_period, score_is_tied

    def set_file_name(self, file_name):
        self.file_name = file_name
